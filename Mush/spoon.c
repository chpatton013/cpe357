#include "spoon.h"

int promptLine(FILE *fp) {
   command **cmds;
   char line[MAX_LINE_LEN], *eof;
   int cmdCount = 0;

   printf(PROMPT);
   fp = fp == NULL ? stdin : fp;
   eof = fgets(line, MAX_LINE_LEN, fp);
   if (!eof) {
      return FALSE;
   }
   line[strlen(line)-1] = '\0';

   cmds = parseCommands(line);
   if (cmds == NULL) {
      return TRUE;
   }
   while (cmds[cmdCount] != NULL) {
      ++cmdCount;
   }

   if (cmdCount) {
      if (!strcmp(cmds[0]->name, "cd")) {
         chdir(cmds[0]->argv[1]);
      }
      else {
         forkCommands(cmds, cmdCount);
      }
   }

   fflush(NULL);
   return TRUE;
}

void forkCommands(command **cmds, int cmdCount) {
   int curCmd, curPipe, status, fid;
   int pipes[MAX_CMDS-1][2];
   pid_t children[MAX_CMDS];
   sigset_t set;

   sigemptyset(&set);
   sigaddset(&set, SIGINT);

   for (curCmd = 0; curCmd < cmdCount; ++curCmd) {
      children[curCmd] = 0;
   }

   for (curCmd = 0; curCmd < cmdCount-1; ++curCmd) {
      if (pipe(pipes[curCmd])) {
         perror("pipe");
         exit(EXIT_FAILURE);
      }
   }

   /* child 0 */
   if ( -1 == (children[0] = fork()) ) {
      perror("fork");
      exit(EXIT_FAILURE);
   }
   if (!children[0]) {
      if (cmdCount > 1) {
         if ( -1 == dup2(pipes[0][WRITE_END], STDOUT_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }

         /* clean up */
         for (curPipe = 0; curPipe < cmdCount-1; ++curPipe) {
            close(pipes[curPipe][READ_END]);
            close(pipes[curPipe][WRITE_END]);
         }
      }
      if (*cmds[0]->inFile) {
         fid = open(cmds[0]->inFile, O_RDONLY);
         if (fid == -1) {
            perror("open");
            exit(EXIT_FAILURE);
         }
         if ( -1 == dup2(fid, STDIN_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }
      }
      if (*cmds[0]->outFile) {
         fid = open(cmds[0]->outFile, CREAT_FLAGS, CREAT_MODE);
         if (fid == -1) {
            perror("open");
            exit(EXIT_FAILURE);
         }
         if ( -1 == dup2(fid, STDOUT_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }
      }

      /* do the exec */
      sigprocmask(SIG_UNBLOCK, &set, NULL);
      execvp(cmds[0]->name, cmds[0]->argv);
      perror("execvp");
      exit(EXIT_FAILURE);
   }

   /* children 1 -> cmdCount-2 */
   for (curCmd = 1; curCmd < cmdCount-1; ++curCmd) {
      if ( -1 == (children[curCmd] = fork()) ) {
         perror("fork");
         exit(EXIT_FAILURE);
      }
      if (!children[curCmd]) {
         if ( -1 == dup2(pipes[curCmd-1][READ_END], STDIN_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }

         if ( -1 == dup2(pipes[curCmd][WRITE_END], STDOUT_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }

         /* clean up */
         for (curPipe = 0; curPipe < cmdCount-1; ++curPipe) {
            close(pipes[curPipe][READ_END]);
            close(pipes[curPipe][WRITE_END]);
         }

         /* do the exec */
         sigprocmask(SIG_UNBLOCK, &set, NULL);
         execvp(cmds[curCmd]->name, cmds[curCmd]->argv);
         perror("execvp");
         exit(EXIT_FAILURE);
      }
   }

   /* child cmdCount-1 */
   if (cmdCount > 1) {
      if ( -1 == (children[curCmd] = fork()) ) {
         perror("fork");
         exit(EXIT_FAILURE);
      }
      if (!children[curCmd]) {
         if ( -1 == dup2(pipes[curCmd-1][READ_END], STDIN_FILENO) ) {
            perror("dup2");
            exit(EXIT_FAILURE);
         }

         if (*cmds[curCmd]->outFile) {
            fid = open(cmds[curCmd]->outFile, CREAT_FLAGS, CREAT_MODE);
            if (fid == -1) {
               perror("open");
               exit(EXIT_FAILURE);
            }
            if ( -1 == dup2(fid, STDOUT_FILENO) ) {
               perror("dup2");
               exit(EXIT_FAILURE);
            }
         }

         /* clean up */
         for (curPipe = 0; curPipe < cmdCount-1; ++curPipe) {
            close(pipes[curPipe][READ_END]);
            close(pipes[curPipe][WRITE_END]);
         }

         /* do the exec */
         sigprocmask(SIG_UNBLOCK, &set, NULL);
         execvp(cmds[curCmd]->name, cmds[curCmd]->argv);
         perror("execvp");
         exit(EXIT_FAILURE);
      }
   }

   for (curPipe = 0; curPipe < cmdCount-1; ++curPipe) {
      close(pipes[curPipe][READ_END]);
      close(pipes[curPipe][WRITE_END]);
   }

   for (curCmd = 0; curCmd < cmdCount; ++curCmd) {
      wait(&status);
   }
}
