#ifndef PARSE_H
#define PARSE_H

#include <ctype.h>
#include "util.h"
#include "command.h"
#include "string_constants.h"

#define NUM_REDIR_ARGS 4

/* Takes an immutable command line string and parses the commands from it 
 * Arguments: char *line = null-terminated line broken by '\n' to be parsed
 * Returns: command*[] representing the commands to be run 
 * REMEMBER to free. */
command **parseCommands(const char *line);

/* Returns: int value of the number of commands based on the number of
 * pipes. */
int getNumCommands(const char *line);

/* Returns: total number of words in line, or -1 if failure */
int parseWords(const char *line);

/* Prints words in line, in quotes and comma separated */
void printWords(int numWords);

/* Initializes the command list to NULL */
void clearCommands(void);

#endif
