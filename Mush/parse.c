#include "parse.h"

static char words[MAX_ARGS*MAX_CMDS][MAX_LINE_LEN];
static command *cmds[MAX_CMDS+1];

command **parseCommands(const char *line) {
   int numWords, curWord, curCmd, argc, numCmds, i;
   char *name, *argv[MAX_ARGS+1], *inFile, *outFile;

   clearCommands();
   /* split line into words */
   numWords = parseWords(line);
   numCmds = getNumCommands(line);

   if (numWords <= 0) {
      fprintf(stderr, S_ERR_NULL_CMD);
      return NULL;
   }

   if (numCmds >= MAX_CMDS) {
      fprintf(stderr, S_ERR_DEEP_PPLN);
      return NULL;
   }

   /* split words into commands */
   curWord = curCmd = 0;
   while (curWord < numWords && curCmd < numCmds) {
      if (strpbrk(words[curWord], TOKENS)) {
         fprintf(stderr, S_ERR_NULL_CMD);
         return NULL;
      }

      /* Initialization */
      inFile = outFile = NULL;
      name = words[curWord];
      argc = 0;
      for (i = curWord; argc <= MAX_ARGS &&
       i <= MAX_ARGS + curWord + NUM_REDIR_ARGS; ++i) {
         if (i == numWords || !strcmp(words[i], "|")) {
            break;
         }
         if (i < numWords && argc == MAX_ARGS && strcmp(words[i+1], "|")) {
            fprintf(stderr, S_ERR_MANY_ARGS(name));
            return NULL;
         }

         else if (!strcmp(words[i], "<")) {
            if (i + 1 == numWords || inFile) {
               fprintf(stderr, S_ERR_IN_REDIR(name));
               return NULL;
            }

            else if (curCmd) {
               fprintf(stderr, S_ERR_AMBIG_IN(name));
               return NULL;
            }

            else {
               inFile = words[++i];
               if (strpbrk(inFile, TOKENS)) {
                  fprintf(stderr, S_ERR_IN_REDIR(name));
                  return NULL;
               }
            }
         }

         else if (!strcmp(words[i], ">")) {
            if (i + 1 == numWords || outFile) {
               fprintf(stderr, S_ERR_OUT_REDIR(name));
               return NULL;
            }

            else if (curCmd < numCmds-1) {
               fprintf(stderr, S_ERR_AMBIG_OUT(name));
               return NULL;
            }

            else {
               outFile = words[++i];
               if (strpbrk(outFile, TOKENS)) {
                  fprintf(stderr, S_ERR_OUT_REDIR(name));
                  return NULL;
               }
            }
         }

         else {
            argv[argc++] = words[i];
         }
      }
      curWord = i+1;
      cmds[curCmd] = init_command(name, argc, argv, inFile, outFile, curCmd);
      ++curCmd;
   }

   return cmds;
}

void clearCommands() {
   int i;
   for (i = 0; i <= MAX_CMDS; ++i) {
      cmds[i] = NULL;
   }
}

void clearArgs(char **argv) {
   int i;
   for (i = 0; i <= MAX_ARGS; ++i) {
      argv[i] = 0;
   }
}

void printWords(int numWords) {
   int i;

   for (i = 0; i < numWords; ++i) {
      printf("\"%s\", ", words[i]);
   }

   printf("\n");
}

int getNumCommands(const char *line) {
   int i, count = 0;

   for (i = 0; line[i] != '\0'; ++i) {
      if (line[i] == '|') {
         ++count;
      }
   }

   return 1 + count;
}

int parseWords(const char *line) {
   int lineNdx = 0, wordNdx, curWord = 0;

   if (strlen(line) > MAX_LINE_LEN) {
      fprintf(stderr, S_ERR_LONG_CMD);
      return -1;
   }

   while (line[lineNdx] != '\0' && curWord <= MAX_ARGS) { 
      /* Remove leading spaces */
      while (isspace(line[lineNdx])) {
         ++lineNdx;
      }

      if (line[lineNdx] == '\0') {
         break;
      }

      wordNdx = 0;

      while (line[lineNdx + wordNdx] != '\0' && 
            !isspace(line[lineNdx + wordNdx]) &&
            wordNdx < MAX_LINE_LEN) {
         words[curWord][wordNdx] = line[lineNdx+wordNdx];
         ++wordNdx;
      }

      words[curWord++][wordNdx] = '\0';

      lineNdx += wordNdx;
   }

   return curWord;
}
