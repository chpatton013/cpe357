#include "command.h"

command *init_command(char *name, int argc, char **argv, char *inFile,
      char *outFile, int pipelineNdx) {
   int i;
   command *cmd = (command*)safe_malloc(sizeof(command));
   strcpy(cmd->name, name);
   cmd->argc = argc;

   if (argc < 0) {
      argc = 0;
   }

   for (i = 0; i < argc; ++i) {
      strcpy(cmd->p_argv[i], argv[i]);
      cmd->argv[i] = cmd->p_argv[i];
   }
   cmd->argv[argc] = NULL;

   if (inFile) {
      strcpy(cmd->inFile, inFile);
   }
   else {
      cmd->inFile[0] = '\0';
   }

   if (outFile) {
      strcpy(cmd->outFile, outFile);
   }
   else {
      cmd->outFile[0] = '\0';
   }

   cmd->pipelineNdx = pipelineNdx;

   return cmd;
}

void free_command(command *cmd) {
   int i;

   if (cmd) {
      if (cmd->name) {
         free(cmd->name);
      }

      if (cmd->argv) {
         for (i = 0; i < cmd->argc; ++i) {
            if (cmd->argv[i]) {
               free(cmd->argv[i]);
            }
         }

         free(cmd->argv);
      }

      if (cmd->inFile) {
         free(cmd->inFile);
      }

      if (cmd->outFile) {
         free(cmd->outFile);
      }

      free(cmd);
   }
}

void printCommand(command *cmd) {
   int i;

   if (!cmd)
      return;

   printf("--------\n");
   printf("Stage %d: \"%s\"\n", cmd->pipelineNdx, cmd->name);
   printf("--------");

   if (*cmd->inFile) {
      printf("\ninput: %s", cmd->inFile);
   }
   else if (!cmd->pipelineNdx) {
      printf("\ninput: stdin");
   }
   else {
      printf("\ninput: pipe from stage %d", cmd->pipelineNdx-1);
   }

   if (*cmd->outFile) {
      printf("\noutput: %s", cmd->outFile);
   }
   else if (cmd->pipelineNdx) {
      printf("\noutput: stdout");
   }
   else {
      printf("\noutput: pipe to stage %d", cmd->pipelineNdx+1);
   }

   printf("\nargc: %d\nargv: ", cmd->argc);

   for (i = 0; i < cmd->argc; ++i) {
      printf("\"%s\", ", cmd->argv[i]);
   }

   printf("\n");
}
