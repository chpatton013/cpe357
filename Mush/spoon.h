#ifndef SPOON_H
#define SPOON_H

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#include "util.h"
#include "string_constants.h"
#include "command.h"
#include "parse.h"

#define READ_END 0
#define WRITE_END 1

#define CREAT_FLAGS O_CREAT | O_TRUNC | O_RDWR
#define CREAT_MODE S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH

/* Takes care of all of the forking and execing.
 * Returns: FALSE if EOF was reached
 * fp: argument to read line from */
int promptLine(FILE *fp);

/* Really does the forking and execing and piping. */
void forkCommands(command **cmds, int cmdCount);

#endif
