#include "main.h"

int main(int argc, char **argv) {
   sigset_t set;
   FILE *fp;

   if (argc > MAX_ARGUMENTS) {
      fprintf(stderr,  "%s\n", USAGE_MSG);
      exit(EXIT_FAILURE);
   }

   fp = (argc == FROM_FILE_ARG + 1) ?
    fopen(argv[FROM_FILE_ARG], "r") : NULL;

   sigemptyset(&set);
   sigaddset(&set, SIGINT);
   sigprocmask(SIG_BLOCK, &set, NULL);

   while (promptLine(fp))
      ;

   if (fp) {
      fclose(fp);
   }
   printf("\n");

   return 0;
}
