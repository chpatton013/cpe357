#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <alloca.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>

#include "string.h"

#define FALSE 0
#define TRUE 1

void *safe_malloc(size_t size);
void *safe_realloc(void *ptr, size_t size);

#endif
