#include "util.h"

void *safe_malloc(size_t size) {
   void *mem = calloc(1, size);

   if (!mem) {
      perror(__FUNCTION__);
      exit(EXIT_FAILURE);
   }

   return mem;
}

void *safe_realloc(void *ptr, size_t size) {
   ptr = realloc(ptr, size);

   if (!ptr) {
      perror(__FUNCTION__);
      exit(EXIT_FAILURE);
   }

   return ptr;
}
