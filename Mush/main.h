#ifndef MAIN_H
#define MAIN_H

#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include "string_constants.h"
#include "spoon.h"

#define MAX_ARGUMENTS 2
#define FROM_FILE_ARG 1

#endif
