#ifndef COMMAND_H
#define COMMAND_H

#include "util.h"

#define MAX_ARGS 10
#define MAX_CMDS 10
#define MAX_LINE_LEN 512

typedef struct command_struct {
   char name[MAX_LINE_LEN]; /* Name of the command itself (ex: ls, pwd, etc.) */

   int argc;    /* arg count for argv list */
   char p_argv[MAX_ARGS][MAX_LINE_LEN]; /* argv list for commands */
   char *argv[MAX_ARGS+1];
   char inFile[MAX_LINE_LEN]; /* Redirected < in file */
   char outFile[MAX_LINE_LEN]; /* Redirected > out file */

   int pipelineNdx; /* 0-based: Where in the pipeline are we? */
} command;

/* Constructor for command */ 
command *init_command(char *name, int argc, char **argv, char *inFile, 
      char *outFile, int pipelineNdx);

/* Destructor for command */
void free_command(command *cmd);

/* Prints: all the details of the command */
void printCommand(command *cmd);

#endif
