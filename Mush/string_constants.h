#ifndef STRING_CONSTANTS_H
#define STRING_CONSTANTS_H

#define USAGE_MSG "usage: mush [ <commandFile> ]"

#define S_ERR_LONG_CMD "command too long\n"
#define S_ERR_DEEP_PPLN "pipeline too deep\n"
#define S_ERR_MANY_ARGS(cmd) "%s: too many arguments\n", cmd
#define S_ERR_NULL_CMD "invalid null command\n"
#define S_ERR_IN_REDIR(cmd) "%s: bad input redirection\n", cmd
#define S_ERR_OUT_REDIR(cmd) "%s: bad output redirection\n", cmd
#define S_ERR_AMBIG_IN(cmd) "%s: ambiguous input redirection\n", cmd
#define S_ERR_AMBIG_OUT(cmd) "%s: ambiguous output redirection\n", cmd
#define TOKENS "<>|"

#define PROMPT "8-P "

#endif
