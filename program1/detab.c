#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define DFLT_TAB_WIDTH 8
#define MIN_TAB_WIDTH 1

int main(int argc, char *argv[])
{
   int tabWidth, tabStop, lineStart, position;
   char curr;

   /* check for specified tab-width */
   if (argc == 1)
      tabWidth = DFLT_TAB_WIDTH;
   else if (argc == 2 && isdigit(argv[1][0])) {
      tabWidth = atoi(argv[1]);
      if (tabWidth < MIN_TAB_WIDTH) {
         fprintf(stderr, "Error: tab-width < %d\n", MIN_TAB_WIDTH);
         exit(-1);
      }
   }
   else {
      fprintf(stderr, "Usage: detab [<tab-width]\n");
      exit(-2);
   }

   lineStart = position = 0;
   while ((curr = getchar()) != EOF) {
      /* check for tab */
      if (curr == '\t') {
         tabStop = tabWidth - (position - lineStart) % tabWidth;
         if (!tabStop)
            tabStop = tabWidth;
         position += tabStop;
         while (tabStop--)
            putchar(' ');
      }
      /* check for backspace */
      else if (curr == '\b' && position != lineStart) {
         putchar(curr);
         --position;
      }
      else {
         /* check for newline */
         if (curr == '\n')
            lineStart = ++position;
         /* check for carriage return */
         else if (curr == '\r')
            position = lineStart;
         /* normal character */
         else
            ++position;
         putchar(curr);
      }
   }

   return 0;
}
