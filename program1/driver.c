#include <stdio.h>
#include <stdlib.h>

#define STRS 10
#define STR_LEN 100

int main(int argc, char *argv[]) {
   FILE *fp;
   char **strs;
   int ndx;

   if (argc != 2) {
      fprintf(stderr, "Usage: driver <filename>\n");
      return -1;
   }
   fp = fopen(argv[1], "w");
   if (!fp) {
      fprintf(stderr, "Error opening file %s for writing.\n", argv[1]);
      return -2;
   }

   strs = malloc(sizeof(char**) * STRS);
   for (ndx = 0; ndx < STRS; ++ndx)
      strs[ndx] = malloc(sizeof(char*) * STR_LEN);

   strs[0] = "\b\b\b12345\b\b\b\b\b12345\b\b\t9";
   strs[1] = "123456789112345678921234567893\r";
   strs[2] = "\b\b\b\b\b\b\b\b\b\b\b\b\t12345";
   strs[3] = "12345678911234567892\r1234567893";
   strs[4] = "";
   strs[5] = "";
   strs[6] = "";
   strs[7] = "";
   strs[8] = "";
   strs[9] = "";

   for (ndx = 0; ndx < STRS; ++ndx)
      fprintf(fp, "%s\n", strs[ndx]);

   return 0;
}
