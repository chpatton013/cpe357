#ifndef MYPWD_H
#define MYPWD_H

#include <limits.h>
#ifndef PATH_MAX
#define PATH_MAX 2048
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <alloca.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#define THIS_DIR "."
#define PARENT_DIR ".."

static char absPath[PATH_MAX + 1] = "";

char *strPrepend(char *base, char *pre);
void statDirR(struct stat *childStat);

#endif
