#include "mypwd.h"

int main(int argc, char **argv) {
   int statErr = 0;
   struct stat *thisStat;

   thisStat = (struct stat*)alloca(sizeof(struct stat));

   /* arg checking */
   if (argc > 1) {
      fprintf(stderr, "usage: %s\n", argv[0]);
      exit(1);
   }

   /* initialize for recursion */
   statErr = lstat(THIS_DIR, thisStat);
   if (statErr) {
      perror("mypwd");
      exit(2);
   }

   statDirR(thisStat);

   printf("%s\n", absPath);

   return 0;
}

void statDirR(struct stat *childStat) {
   int statErr, searchErr = 1;
   struct stat *thisStat, *tempStat;
   DIR *thisDir;
   struct dirent *entry;
   
   thisStat = (struct stat*)alloca(sizeof(struct stat));
   tempStat = (struct stat*)alloca(sizeof(struct stat));

   chdir(PARENT_DIR);
   statErr = lstat(THIS_DIR, thisStat);
   if (statErr) {
      perror("mypwd");
      exit(3);
   }

   /* check for root */
   if (thisStat->st_ino == childStat->st_ino &&
    thisStat->st_dev == childStat->st_dev) {
      return;
   }

   /* find correct dirent */
   thisDir = opendir(THIS_DIR);
   while ((entry = readdir(thisDir))) {
      statErr = lstat(entry->d_name, tempStat);
      if (statErr) {
         perror("mypwd");
         exit(3);
      }
      /* matching inode && device id */
      if (tempStat->st_ino == childStat->st_ino &&
       tempStat->st_dev == childStat->st_dev) {
         searchErr = 0;

         if (strlen(absPath) + strlen("/") + strlen(entry->d_name) > PATH_MAX) {
            printf("mypwd: path too long\n");
            exit(4);
         }
         else {
            strPrepend(absPath, entry->d_name);
            strPrepend(absPath, "/");
         }

         /* recurse */
         statDirR(thisStat);
         break;
      }
   }
   if (searchErr) {
      printf("mypwd: could not get current directory\n");
      exit(5);
   }
}

char *strPrepend(char *base, char *pre) {
   int ndx, baseLen, preLen;

   baseLen = strlen(base);
   preLen = strlen(pre);

   for (ndx = baseLen; ndx >= 0; --ndx)
      base[ndx + preLen] = base[ndx];

   for (ndx = 0; ndx < preLen; ++ndx)
      base[ndx] = pre[ndx];

   return base;
}
