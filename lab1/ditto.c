#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define PATH_LEN 500

void main(int argc, char *argv[])
{
   char direc[PATH_LEN];

   strcpy(direc, getenv("PWD"));

   execvp(direc, argv);
}
