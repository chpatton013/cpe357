#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main (int argc, char **argv) {
   int st = -1;
   pid_t child;

   if (argc != 2) {
      fprintf(stderr, "usage: tryit command\n");
      exit(EXIT_FAILURE);
   }

   child = fork();
   if (child) {
      wait(&st);
      if (st) {
         printf("Process %d exited with an erro value.\n", child);
      }
      else {
         printf("Process %d succeeded.\n");
      }
   }
   else {
      execl(argv[1], argv[0], (char *)NULL);
      perror(argv[1]);
   }

   return st;
}
