#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
   pid_t pid;

   printf("Hello, world!\n");

   pid = fork();

   if (pid == 0) {
      printf("This is the parent, pid %d.\n", pid);
      wait();
   }
   else {
      printf("This is the child, pid %d.\n", pid);
      exit(EXIT_SUCCESS);
   }

   printf("This is the parent, pid %d, signing off.\n", pid);

   return 0;
}
