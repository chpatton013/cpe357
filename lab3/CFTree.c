#include "CFTree.h"

CFTree * constructCFTree(CFLL *ll) {
   CFTree *tree = (CFTree*)__malloc(sizeof(CFTree));
   CFNode *nd, *nd1, *nd2;

   if (ll) {
      while (ll->size > 1) {
         nd1 = removeCFNode(ll);
         nd2 = removeCFNode(ll);
         nd = constructCFNode(NULL);

         nd->left = nd1;
         nd->right = nd2;
         nd->value = nd1->value + nd2->value;
         placeCFNode(ll, nd);
      }
      tree->root = removeCFNode(ll);

      makeTreePrefixes(tree);
   }

   return tree;
}

void freeBranchNodes(CFNode *nd) {
   if (nd) {
      if (nd->left)
         freeBranchNodes(nd->left);
      if (nd->right)
         freeBranchNodes(nd->right);
      if (nd->left && nd->right)
         freeCFNode(nd);
   }
}

void freeCFTree(CFTree *tree) {
   if (tree)
      freeBranchNodes(tree->root);
   free(tree);
}

void makeNodePrefixes(CFNode *nd, char *prefix) {
   char *left, *right;
   int len;

   if (nd) {
      len = strlen(prefix);

      if (nd->cf) {
         nd->cf->prefix = (char*)__malloc(sizeof(char) * len);
         strcpy(nd->cf->prefix, prefix);
      }
      else {
         left = (char*)alloca(sizeof(char) * (len + 2));
         strcpy(left, prefix);
         left[len] = '0';

         right = (char*)alloca(sizeof(char) * (len + 2));
         strcpy(right, prefix);
         right[len] = '1';

         left[len + 1] = right[len + 1] = 0;

         makeNodePrefixes(nd->left, left);
         makeNodePrefixes(nd->right, right);
      }
   }
}

void makeTreePrefixes(CFTree *tree) {
   char *prefix;

   if (tree) {
      prefix = (char*)alloca(sizeof(char));
      prefix[0] = 0;
      makeNodePrefixes(tree->root, prefix);
   }
}
