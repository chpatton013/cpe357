#ifndef MAIN_H
#define MAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "chpatton.h"
#include "CharFreq.h"
#include "CFNode.h"
#include "CFLL.h"
#include "CFTree.h"

#define MIN_ARGS 1
#define MAX_ARGS INT_MAX
#define IN_ARG 1
#define USAGE_MSG "Usage: htable <filename>"

void makePrefixes(CharFreq **tbl);

#endif
