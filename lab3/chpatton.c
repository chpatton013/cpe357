#include "chpatton.h"

void * __malloc(size_t size) {
   unsigned int ndx;
   void *ptr = malloc(size);

   if (!ptr) {
      perror(__FUNCTION__);
      exit(-1);
   }

   for (ndx = 0; ndx < size; ++ndx)
      ((char*)ptr)[ndx] = 0;

   return ptr;
}
