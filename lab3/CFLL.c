#include "CFLL.h"

CFLL * constructCFLL(CFNode **arr, int size) {
   int ndx;
   CFNode *nd;
   CFLL *ll = (CFLL*)__malloc(sizeof(CFLL));

   if (!arr || !size) {
      ll->head = NULL;
      ll->size = 0;
   }
   else {
      for (ll->head = arr[0], nd = ll->head, ll->size = ndx = 1;
       ndx < size; ++ndx, nd = nd->next, ++ll->size)
         nd->next = arr[ndx];
   }

   return ll;
}

void freeCFLL(CFLL *ll) {
   if (ll)
      free(ll);
}

CFNode * removeCFNode(CFLL *ll) {
   CFNode *nd;

   if (ll && ll->head) {
      nd = ll->head;
      ll->head = ll->head->next;
      --ll->size;
      return nd;
   }
   else
      return NULL;
}

void placeCFNode(CFLL *ll, CFNode *node) {
   CFNode *nd, *prev;

   if (ll) {
      if (cfnComp(&node, &ll->head) < 0) {
         node->next = ll->head;
         ll->head = node;
      }
      else if (!ll->head) {
         ll->head = node;
      }
      else { 
         for (prev = ll->head, nd = prev->next; nd && cfnComp(&node, &nd) > 0;
          prev = nd, nd = prev->next)
            ;
         prev->next = node;
         node->next = nd;
      }
      ++ll->size;
   }
}
