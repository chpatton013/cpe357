#include "main.h"

int main(int argc, char **argv) {
   FILE *in;
   CharFreq **tbl;

   /* error checking */
   if (argc < MIN_ARGS || argc > MAX_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(-1);
   }
   
   /* get args */
   if (argc > MIN_ARGS) {
      in = fopen(argv[IN_ARG], "r");
      if (!in)
         perror(argv[IN_ARG]);
   }
   else
      in = stdin;

   /* create and fill table */
   tbl = constructCFTable();
   fillCFTable(tbl, in);

   /* optimize mapping */
   makePrefixes(tbl);

   /* print table and cleanup */
   printCFTable(tbl);
   freeCFTable(tbl);

   return 0;
}

void makePrefixes(CharFreq **tbl) {
   int ndx1, ndx2, tblSize;
   CFNode **arr;
   CFLL *ll;
   CFTree *tree;

   tbl = copyCFTable(tbl);

   qsort(tbl, TABLE_SIZE, sizeof(CharFreq*), cfComp);

   for (ndx1 = 0; ndx1 < TABLE_SIZE; ++ndx1)
      if (tbl[ndx1]->freq)
         break;
   tblSize = TABLE_SIZE - ndx1;

   arr = (CFNode**)alloca(sizeof(CFNode*) * tblSize);
   for (ndx2 = 0; ndx2 < tblSize; ++ndx1, ++ndx2)
      arr[ndx2] = constructCFNode(tbl[ndx1]);

   free(tbl);

   ll = constructCFLL(arr, tblSize);

   tree = constructCFTree(ll);

   freeCFTree(tree);
   freeCFLL(ll);
   for (ndx1 = 0; ndx1 < tblSize; ++ndx1)
      freeCFNode(arr[ndx1]);
}
