#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "other.h"

#define FIVE 5

int main(int argc, char **argv) {
   int val = argc > 1 && isdigit(argv[1][0]) ? atoi(argv[1]) : FIVE;
   printf("val = %d\n", val);
   return 0;
}
