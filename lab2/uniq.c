#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *readLongLine(FILE *file) {
   char c, *str = (char*)malloc(sizeof(char));
   int size = 0;

   /* inefficient reallocation for strings of any length */
   *str = 0;
   while (fscanf(file, "%c", &c), c != EOF && c != '\n') {
      str[size] = c;
      str = (char*)realloc(str, sizeof(char) * ++size);
      str[size] = 0;
   }

   return str;
}

int main (void) {
   char *str, *cache = (char*)malloc(0);

   while (strlen(str = readLongLine(stdin)) > 0 && str[0] != EOF) {
      if (strcmp(str, cache)) {
         printf("%s\n", str);
         free(cache);
         cache = str;
      }
   }

   return 0;
}
