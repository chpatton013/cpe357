#include "WordFreqLL.h"

WordFreqLL * constructWordFreqLL() {
   WordFreqLL *ll = (WordFreqLL*)malloc(sizeof(WordFreqLL));

   ll->head = (WordFreqNode*)malloc(sizeof(WordFreqNode));
   ll->head->next = NULL;
   ll->size = 0;

   return ll;
}

/**
 * checks LL for string.
 * if not there, adds a new node to end and returns 1.
 * if there, increments node->freq and return 0.
 */
int addToWordFreqLL(char *str, WordFreqLL *ll) {
   WordFreqNode *nd, *prev;

   prev = ll->head;
   nd = prev->next;

   for (; nd != NULL; prev = nd, nd = nd->next) {
      if (!strcmp(nd->str, str)) {
         ++(nd->freq);
         return 0;
      }
   }
   prev->next = (WordFreqNode*)malloc(sizeof(WordFreqNode));
   prev->next->str = str;
   prev->next->freq = 1;

   ++ll->size;

   return 1;
}

/**
 * returns deep array of nodes in ll
 */
WordFreqNode ** wordFreqLLToArray(WordFreqLL *ll) {
   int ndx;
   WordFreqNode *nd,
    **arr = (WordFreqNode**)malloc(sizeof(WordFreqNode*) * ll->size);

   for (ndx = 0, nd = ll->head->next; ndx < ll->size; ++ndx, nd = nd->next)
      arr[ndx] = cloneWordFreqNode(nd);

   return arr;
}

void freeWordFreqLL(WordFreqLL *ll) {
   WordFreqNode *nd, *next;
   for (nd = ll->head; nd != NULL; nd = next) {
      next = nd->next;
      freeWordFreqNode(nd);
   }
   free(ll);
}
