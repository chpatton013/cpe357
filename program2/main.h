/**
 * CREDITS: strHash taken from Dan Bernstein
 */
#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "WordFreqNode.h"
#include "WordFreqLL.h"
#include "WordFreqHashTable.h"

#define USAGE_MSG "usage: fw [-n num] [ file1 [ file2 ...] ]"
#define DFLT_WORD_SIZE 8
#define DFLT_WORDS 10
#define MIN_WORDS 1


char * getWord(FILE *fp);
int wordFreqCompare(const void *wf1, const void *wf2);

#endif
