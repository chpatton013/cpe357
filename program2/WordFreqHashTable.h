#ifndef WORD_FREQ_HASH_TABLE_H
#define WORD_FREQ_HASH_TABLE_H

#include <math.h>

#include "WordFreqNode.h"
#include "WordFreqLL.h"

#define DFLT_TABLE_SIZE 50021
#define CHECK_DEPTH 10
#define GOOD_DEPTH 5

typedef struct WordFreqHashTable {
   WordFreqLL **table;
   int size, length;
} WordFreqHashTable;

WordFreqHashTable * constructWordFreqHashTable();
void addToWordFreqHashTable(char *str, WordFreqHashTable *tbl);
WordFreqHashTable * resizeWordFreqHashTable(WordFreqHashTable *tbl, int size);
WordFreqNode ** wordFreqHashTableToArray(WordFreqHashTable *tbl);
void freeWordFreqHashTable(WordFreqHashTable *tbl);
unsigned int strHash(char *str);
int maxDepth(WordFreqHashTable *tbl);
int isPrime(int num);
int nextPrime(int num);
int prevPrime(int num);

#endif
