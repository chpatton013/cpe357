#include "WordFreqNode.h"

void freeWordFreqNode(WordFreqNode * node) {
   free(node->str);
   free(node);
}

/* deep-copy of node */
WordFreqNode * cloneWordFreqNode(WordFreqNode * node) {
   WordFreqNode *nd = (WordFreqNode*)malloc(sizeof(WordFreqNode));

   nd->str = (char*)malloc(sizeof(char) * (strlen(node->str) + 1));
   strcpy(nd->str, node->str);

   nd->freq = node->freq;

   return nd;
}
