#ifndef WORD_FREQ_NODE_H
#define WORD_FREQ_NODE_H

#include <stdlib.h>
#include <string.h>

typedef struct Node {
   char *str;
   int freq;
   struct Node * next;
} WordFreqNode;

void freeWordFreqNode(WordFreqNode * node);
WordFreqNode * cloneWordFreqNode(WordFreqNode * node);

#endif
