#include <stdio.h>
#include "WordFreqHashTable.h"

WordFreqHashTable * constructWordFreqHashTable() {
   int ndx;
   WordFreqHashTable *tbl =
    (WordFreqHashTable*)malloc(sizeof(WordFreqHashTable));

   tbl->table = (WordFreqLL**)malloc(sizeof(WordFreqLL*) * DFLT_TABLE_SIZE);
   for (ndx = 0; ndx < DFLT_TABLE_SIZE; ++ndx)
      tbl->table[ndx] = NULL;

   tbl->size = 0;
   tbl->length = DFLT_TABLE_SIZE;

   return tbl;
}

/**
 * adds str to hashed LL.
 * if not there, adds it to LL and increments tbl->size.
 * if there, increments node->freq.
 *
 * NOTE: removed resizing to prevent seg faults. not sure why it break at random
 * resizes, so avoiding the problem entirely.
 */
void addToWordFreqHashTable(char *str, WordFreqHashTable *tbl) {
   static int adds = 0;
   int hash, result;

   /*
   if (!(adds % CHECK_DEPTH) && maxDepth(tbl) > GOOD_DEPTH)
      tbl = resizeWordFreqHashTable(tbl, nextPrime(tbl->length));
   */

   hash = strHash(str) % tbl->length;

   if (!tbl->table[hash])
      tbl->table[hash] = constructWordFreqLL();

   result = addToWordFreqLL(str, tbl->table[hash]);

   if (result) {
      ++adds;
      ++tbl->size;
   }
}

/**
 * resizes hash table to size given (unless new size == old size), then returns
 * tbl.
 *
 * NOTE: currently broken. use at own risk. improper memory allocation causes
 * random seg faults.
 */
WordFreqHashTable * resizeWordFreqHashTable(WordFreqHashTable *tbl, int size) {
   int ndx;
   WordFreqNode **arr;

   if (tbl->size == size)
      return tbl;

   arr = wordFreqHashTableToArray(tbl);

   for (ndx = 0; ndx < tbl->length; ++ndx)
      if (tbl->table[ndx])
         freeWordFreqLL(tbl->table[ndx]);
   free(tbl->table);

   tbl->size = 0;
   tbl->length = nextPrime(tbl->length * 2);
   tbl->table = (WordFreqLL**)malloc(sizeof(WordFreqLL*) * tbl->length);

   for (ndx = 0; ndx < tbl->length; ++ndx)
      addToWordFreqHashTable(arr[ndx]->str, tbl);

   return tbl;
}

/**
 * returns a deep array of all nodes in tbl
 */
WordFreqNode ** wordFreqHashTableToArray(WordFreqHashTable *tbl) {
   int tblNdx, llNdx, arrNdx;
   WordFreqNode **temp, **arr;

   arr = (WordFreqNode**)malloc(sizeof(WordFreqNode*) * tbl->size);

   for (tblNdx = 0, arrNdx = 0; tblNdx < tbl->length; ++tblNdx) {
      if (tbl->table[tblNdx]) {
         temp = wordFreqLLToArray(tbl->table[tblNdx]);
         for (llNdx = 0; llNdx < tbl->table[tblNdx]->size; ++llNdx, ++arrNdx)
            arr[arrNdx] = temp[llNdx];
         free(temp);
      }
   }

   return arr;
}

void freeWordFreqHashTable(WordFreqHashTable *tbl) {
   int ndx;

   for (ndx = 0; ndx < tbl->length; ++ndx)
      if (tbl->table[ndx])
         freeWordFreqLL(tbl->table[ndx]);

   free(tbl->table);
   free(tbl);
}

/**
 * string hash function taken from Dan Bernstein
 */
unsigned int strHash(char *str) {
   unsigned int hash = 0;

   while (*str)
      hash = ((hash << 5) + hash) ^ *str++;

   return hash;
}

int maxDepth(WordFreqHashTable *tbl) {
   int ndx, max = 0;

   for(ndx = 0; ndx < tbl->length; ++ndx)
      if (tbl->table[ndx] && max < tbl->table[ndx]->size)
         max = tbl->table[ndx]->size;

   return max;
}

int isPrime(int num) {
   int ndx, max = ceil(sqrt(num));

   for (ndx = 2; ndx < max && num % ndx; ++ndx)
      ;

   return (--ndx < max);
}

int nextPrime(int num) {
   while (!isPrime(num))
      ++num;
   return num;
}

int prevPrime(int num) {
   while (!isPrime(num))
      --num;
   return num;
}

