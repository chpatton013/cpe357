#include "main.h"

int main(int argc, char **argv) {
   int ndx, numWords, bound;
   char *word;
   FILE *file;
   WordFreqNode **wfArr;
   WordFreqHashTable *table;

   numWords = DFLT_WORDS;
   table = constructWordFreqHashTable();

   /* argument checking */
   /* check for stdin*/
   if (argc == 1)
      while ((word = getWord(stdin)))
         addToWordFreqHashTable(word, table);

   /* begin checking argv's and process files */
   for (ndx = 1; ndx < argc; ++ndx) {
      /* if -n secified */
      if (!strcmp(argv[ndx], "-n")) {
         /* check for number */
         if (argc > ndx + 1 && isdigit(argv[ndx + 1][0])) {
            numWords = atoi(argv[++ndx]);

            /* check for stdin */
            if (argc == 3)
               while ((word = getWord(stdin)))
                  addToWordFreqHashTable(word, table);
         }
         else
            fprintf(stderr, "%s\n", USAGE_MSG), exit(-1);
      }
      /* read from files */
      else {
         file = fopen(argv[ndx], "r");

         if (file) {
            while ((word = getWord(file)))
               addToWordFreqHashTable(word, table);

            fclose(file);
         }
         else
            perror(argv[ndx]);
      }
   }

   /* sort results */
   wfArr = wordFreqHashTableToArray(table);
   qsort((void**)wfArr, table->size, sizeof(WordFreqNode*), (*wordFreqCompare));

   /* print results */
   printf("The top %d words (out of %d) are:\n", numWords, table->size);
   bound = (numWords > table->size) ? table->size : numWords;
   for (ndx = 0; ndx < bound; ++ndx)
      printf("%9d %s\n", wfArr[ndx]->freq, wfArr[ndx]->str);

   /* cleanup */
   if (wfArr) {
      for (ndx = 0; ndx < table->size; ++ndx)
         freeWordFreqNode(wfArr[ndx]);
      free(wfArr);
   }
   freeWordFreqHashTable(table);

   return 0;
}

char * getWord(FILE *fp) {
   char *str;
   int ndx, c, size = DFLT_WORD_SIZE;

   /* burn non-alphabetic */
   for (ndx = 0; !isalpha((char)(c = fgetc(fp))); ++ndx)
      if (c == EOF)
         return NULL;

   str = (char*)malloc(sizeof(char) * DFLT_WORD_SIZE);
   str[0] = (char)tolower((int)c);

   /* read word */
   for (ndx = 1; isalpha((char)(c = fgetc(fp))) && c != EOF; ++ndx) {
      if (ndx == size - 1)
         str = realloc(str, sizeof(char) * (size *= 2));
      str[ndx] = (char)tolower((int)c);
   }

   str[ndx] = '\0';

   return str;
}

int wordFreqCompare(const void *wf1, const void *wf2) {
   int val = ((WordFreqNode*)*(WordFreqNode**)wf2)->freq -
    ((WordFreqNode*)*(WordFreqNode**)wf1)->freq;
   return val ? val :
    strcmp(((WordFreqNode*)*(WordFreqNode**)wf2)->str,
    ((WordFreqNode*)*(WordFreqNode**)wf1)->str);
}
