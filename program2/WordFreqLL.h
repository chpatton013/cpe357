#ifndef WORD_FREQ_LL_H
#define WORD_FREQ_LL_H

#include <stdlib.h>
#include <string.h>

#include "WordFreqNode.h"

typedef struct WordFreqLL {
   WordFreqNode *head;
   int size;
} WordFreqLL;

WordFreqLL * constructWordFreqLL();
int addToWordFreqLL(char *str, WordFreqLL *ll);
WordFreqNode ** wordFreqLLToArray(WordFreqLL *ll);
void freeWordFreqLL(WordFreqLL *ll);

#endif
