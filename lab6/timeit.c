#include "timeit.h"

void handler(int signum) {
   static int it = 0;

   signum &= 1;

   if (++it % 2)
      printf("Tick...");
   else
      printf("Tock\n");
   fflush(stdout);
}

void timer(int n) {
   int ndx;
   struct sigaction sa;
   struct itimerval itv;

   sa.sa_handler = handler;
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   if (sigaction(SIGALRM, &sa, NULL)) {
      perror("sigaction");
      exit(1);
   }

   itv.it_interval.tv_sec = 0;
   itv.it_interval.tv_usec = USEC_PER_SEC / 2;
   itv.it_value.tv_sec = 0;
   itv.it_value.tv_usec = USEC_PER_SEC / 2;
   if (setitimer(ITIMER_REAL, &itv, NULL)) {
      perror("setitimer");
      exit(1);
   }

   for(ndx = 0, n += n; ndx < n; ++ndx)
      pause();

   printf("Time's up!\n");
}

int main(int argc, char **argv) {
   int ndx, count;

   if (argc < MIN_ARGS || argc > MAX_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(1);
   }

   for (ndx = 0; argv[COUNT][ndx]; ++ndx) {
      if (!isdigit(argv[COUNT][ndx])) {
         fprintf(stderr, "%s: malformed time\n", argv[COUNT]);
         exit(2);
      }
   }
   count = atoi(argv[COUNT]);

   timer(count);

   return 0;
}
