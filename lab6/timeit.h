#ifndef TIMEIT_H
#define TIMEIT_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <sys/time.h>
#include <unistd.h>

#define MIN_ARGS 2
#define MAX_ARGS 2
#define USAGE_MSG "Usage: timeit <count>"

#define COUNT 1

#define USEC_PER_SEC 1000000

#endif
