#ifndef EXTRACT_H
#define EXTRACT_H

#include "header.h"
#include "file.h"

extern char *execName, *arcName, **pathNames;
extern int strict, numPaths;

void extract();

#endif
