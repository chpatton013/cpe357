#include "extract.h"

void extract() {
   char flag;
   int fdi = open(arcName, O_RDONLY);
   Header *h;

   if (!fdi) {
      perror(execName);
      exit(1);
   }

   h = constructHeader();

   flag = 1;
   while(flag) {
      if (readHeader(h, fdi)) {
         if (!flag) break;
         else flag = 0;
      }
      else readFile(h, fdi);
   }

   free(h);
   close(fdi);
}
