#include "file.h"

void readFile(Header *h, int fdi) {
   int fdo = 0, leftover, blocksLeft;
   unsigned char bf[BLOCK_SIZE];
   char *link;
   char *name = (char*)alloca(sizeof(char) * (NAME_LEN + 1)),
    *prefix = (char*)alloca(sizeof(char) * (PREFIX_LEN + 1)),
    *temp1 = (char*)alloca(sizeof(char) * (PREFIX_LEN + NAME_LEN + 2)), *temp2;

   strcap(name, h->name, NAME_LEN);
   strcap(prefix, h->prefix, PREFIX_LEN);

   temp1[0] = 0;
   if (strlen(prefix)) {
      strcpy(temp1, prefix);
      strcat(temp1, "/");
   }
   strcat(temp1, name);

   /* skip files not in specified path */
   if (path && !isInPaths(temp1, pathNames, numPaths)) {
      lseek(fdi, ceil(h->size / (double)BLOCK_SIZE) * BLOCK_SIZE, SEEK_CUR);
      return;
   }

   /* strict conformance */
   if (strict && (strncmp(h->magic, "ustar", MAGIC_LEN) ||
    h->magic[MAGIC_LEN - 1] || strncmp(h->version, "00", VERSION_LEN)))
      return;

   /* directory */
   if (h->typeflag == TF_DIR) {
      if (mkdir(temp1, h->mode)) {
         perror(execName);
         exit(1);
      }
   }
   /* symlink */
   else if (h->typeflag == TF_LNK) {
      link = (char*)alloca(sizeof(char) * (LINKNAME_LEN + 1));
      strcap(link, h->linkname, LINKNAME_LEN);

      temp2 = (char*)alloca(sizeof(char) * (PREFIX_LEN + LINKNAME_LEN + 2));
      temp2[0] = 0;
      if (strlen(prefix)) {
         strcpy(temp2, prefix);
         strcat(temp2, "/");
      }
      strcat(temp1, link);

      if (symlink(temp1, temp2)) {
         perror(execName);
         exit(2);
      }
   }
   /* regular file */
   else if (!(fdo = open(temp1, O_WRONLY | O_CREAT | O_TRUNC, h->mode))) {
      perror(execName);
      exit(3);
   }

   /* write file blocks (nothing if size == 0: dir, link, empty files) */
   blocksLeft = h->size / BLOCK_SIZE;
   while (blocksLeft-- > 0) {
      read(fdi, bf, BLOCK_SIZE);
      write(fdo, bf, BLOCK_SIZE);
   }
   leftover = h->size % BLOCK_SIZE;
   if (leftover) {
      read(fdi, bf, BLOCK_SIZE);
      write(fdo, bf, leftover);
   }

   if (fdo)
      close(fdo);
}

void writeFile(Header *h, int fdi, int fdo) {
   /* create unsupported */
   h = NULL;
   fdi = fdo = 0;
}
