#ifndef TABLE_H
#define TABLE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>

#include "header.h"

extern char *execName, *arcName, **pathNames;
extern int verbose, path, numPaths;

void table();

#endif
