#include "table.h"

void table() {
   char flag;
   char *name = (char*)alloca(sizeof(char) * (NAME_LEN + 1)),
    *prefix = (char*)alloca(sizeof(char) * (PREFIX_LEN + 1)),
    *temp = (char*)alloca(sizeof(char) * (NAME_LEN + PREFIX_LEN + 2));
   int fdi = open(arcName, O_RDONLY);
   Header *h;

   if (!fdi) {
      perror(execName);
      exit(1);
   }

   h = constructHeader();

   flag = 1;
   while(flag) {
      if (readHeader(h, fdi)) {
         if (!flag) break;
         else flag = 0;
      }
      else {
         strcap(name, h->name, NAME_LEN);
         strcap(prefix, h->prefix, PREFIX_LEN);

         temp[0] = 0;
         if (strlen(prefix)) {
            strcpy(temp, prefix);
            strcat(temp, "/");
         }
         strcat(temp, name);

         if (!path || isInPaths(temp, pathNames, numPaths))
            printHeader(h, verbose);
      }

      lseek(fdi, ceil(h->size / (double)BLOCK_SIZE) * BLOCK_SIZE, SEEK_CUR);
   }

   free(h);
   close(fdi);
}
