#ifndef MAIN_H
#define MAIN_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "create.h"
#include "table.h"
#include "extract.h"
#include "header.h"

#define MIN_ARGS 3
#define MAX_ARGS 4

#define USAGE_MSG "USAGE: [ctxvS]f tarfile [path [...]]"
#define MODE_MSG "exactly one mode must be specified"
#define STRICT_MSG "strict mode can only be specified for extraction"

#define OPTIONS 1
#define FILENAME 2
#define PATHNAME 3
#define MODES "ctx"

int verbose = 0, strict = 0, path = 0, numPaths = 0;
char *execName, *arcName = "", **pathNames = 0;

#endif
