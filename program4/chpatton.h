#ifndef CHPATTON_H
#define CHPATTON_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

size_t strcap(char *to, const char *from, size_t num);
void * __malloc(size_t size);
int pathNTail(char *to, const char *from, int n);
char *intToOctStr(unsigned int i, char *to, int n, int m);
uint64_t octStrToUInt(char *from, int n);
int isInPaths(char *file, char **paths, int n);

#endif
