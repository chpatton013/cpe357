#ifndef HEADER_H
#define HEADER_H

#include <stdint.h>
#include <stdlib.h>
#include <alloca.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#include "chpatton.h"

#define BLOCK_SIZE 512
#define STR_BUFFER 1024
#define STR_LEN 256
#define TEMP_LEN 18
#define PERM_LEN 9
#define TIME_STR_LEN 16

#define TF_DIR '5'
#define TF_LNK '2'

#define SEC_PER_MIN 60
#define MINS_PER_HR 60
#define HRS_PER_DAY 24
#define DAYS_PER_MNT 30
#define MNTS_PER_YR 12

#define NAME_OFF 0
#define NAME_LEN 100
#define MODE_OFF 100
#define MODE_LEN 8
#define UID_OFF 108
#define UID_LEN 8
#define GID_OFF 116
#define GID_LEN 8
#define SIZE_OFF 124
#define SIZE_LEN 12
#define MTIME_OFF 136
#define MTIME_LEN 12
#define CHKSUM_OFF 148
#define CHKSUM_LEN 8
#define TYPEFLAG_OFF 156
#define TYPEFLAG_LEN 1
#define LINKNAME_OFF 157
#define LINKNAME_LEN 100
#define MAGIC_OFF 257
#define MAGIC_LEN 6
#define VERSION_OFF 263
#define VERSION_LEN 2
#define UNAME_OFF 265
#define UNAME_LEN 32
#define GNAME_OFF 297
#define GNAME_LEN 32
#define DEVMAJOR_OFF 329
#define DEVMAJOR_LEN 8
#define DEVMINOR_OFF 337
#define DEVMINOR_LEN 8
#define PREFIX_OFF 345
#define PREFIX_LEN 155

extern char *execName, *arcName, **pathNames;
extern int verbose, strict, path, numPaths;

typedef struct Header {
   char typeflag;

   char name[NAME_LEN],
        linkname[LINKNAME_LEN],
        magic[MAGIC_LEN],
        version[VERSION_LEN],
        uname[UNAME_LEN],
        gname[GNAME_LEN],
        prefix[PREFIX_LEN];

   uint32_t mode,
            uid,
            gid,
            chksum,
            devmajor,
            devminor;

   size_t size;
   time_t mtime;
} Header;

int isEmpty(unsigned char *bf, int n);
Header *constructHeader();
int readHeader(Header *h, int fd);
void writeHeader(Header *h, int fd);
void printHeader(Header *h, int v);

#endif
