#ifndef FILE_H
#define FILE_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "chpatton.h"
#include "header.h"

void readFile(Header *h, int fdi);
void writeFile(Header *h, int fdi, int fdo);

#endif
