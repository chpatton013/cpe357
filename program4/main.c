#include "main.h"

int validTarFile(char *fileName) {
   unsigned char bf[BLOCK_SIZE * 2];
   int fd;

   if (!(fd = open(fileName, O_RDONLY)))
      return 0;
   lseek(fd, BLOCK_SIZE * -2, SEEK_END);

   read(fd, bf, BLOCK_SIZE * 2);

   return isEmpty(bf, BLOCK_SIZE * 2);
}

int main(int argc, char **argv) {
   int ndx, temp;
   char mode, file;

   /* global executable name */
   execName = argv[0];

   /* mode switchers */
   mode = file = 0;

   /* argument checking */
   /* arg range */
   if (argc < MIN_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(1);
   }

   /* arg declaration */
   temp = strlen(argv[OPTIONS]);
   for (ndx = 0; ndx < temp; ++ndx) {
      if (strchr(MODES, argv[OPTIONS][ndx])) {
         if (mode) {
            fprintf(stderr, "%s\n", MODE_MSG);
            exit(2);
         }
         else
            mode = argv[OPTIONS][ndx];
      }
      else if (argv[OPTIONS][ndx] == 'v') verbose = 1;
      else if (argv[OPTIONS][ndx] == 'S') strict = 1;
      else if (argv[OPTIONS][ndx] == 'f') file = 1;
      else if (argv[OPTIONS][ndx] != '-') {
         fprintf(stderr, "%s\n", USAGE_MSG);
         exit(3);
      }
   }
   if (!mode) {
      fprintf(stderr, "%s\n", MODE_MSG);
      exit(5);
   }
   if (strict && mode != 'x') {
      fprintf(stderr, "%s\n", STRICT_MSG);
      exit(6);
   }
   if (!file) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(7);
   }
   arcName = argv[FILENAME];
   if (argc > PATHNAME) {
      path = 1;
      pathNames = (char**)alloca(sizeof(char*) * argc - PATHNAME);
      for (numPaths = 0; numPaths < argc - PATHNAME; ++numPaths)
         pathNames[numPaths] = argv[numPaths + PATHNAME];
   }

   if (!validTarFile(arcName)) {
      fprintf(stderr, "%s: This does not look like a tar archive", execName);
      exit(8);
   }

   /* do operation */
   if (mode == 'c')
      create();
   else if (mode == 't')
      table();
   else
      extract();

   return 0;
}
