#include "chpatton.h"

size_t strcap(char *to, const char *from, size_t num) {
   size_t ndx = 0;

   for (ndx = 0; from[ndx] && ndx < num; ++ndx)
      to[ndx] = from[ndx];

   to[ndx] = 0;

   return ndx;
}

void * __malloc(size_t size) {
   unsigned int ndx;
   void *ptr = malloc(size);

   if (!ptr) {
      perror(__FUNCTION__);
      exit(1);
   }

   for (ndx = 0; ndx < size; ++ndx)
      ((char*)ptr)[ndx] = 0;

   return ptr;
}

int pathNTail(char *to, const char *from, int n) {
   int start, ndx;

   for (start = ndx = 0; from[ndx]; ++ndx)
      if (from[ndx] == '/' && from[ndx + 1])
         start = ndx + 1;

   for (ndx = 0; ndx < n && from[start + ndx]; ++ndx)
      to[ndx] = from[start + ndx];

   if (ndx < n)
      to[ndx] = 0;

   return ndx;
}

char *intToOctStr(unsigned int i, char *to, int n, int m) {
   int ndx;
   unsigned int mask = 7;

   for (ndx = 0; ndx < n; ++ndx, i >>= 3)
      to[n - ndx - 1] = (i & mask) + '0';

   for (; ndx < m; ++ndx)
      to[ndx] = 0;

   return to;
}

uint64_t octStrToUInt(char *from, int n) {
   int ndx;
   uint64_t i;

   ndx = strlen(from);
   if (ndx < n) n = ndx;

   for (i = ndx = 0; from[ndx] && ndx < n; ++ndx) {
      i <<= 3;
      i |= (from[ndx] - '0');
   }

   return i;
}

int isInPaths(char *file, char **paths, int n) {
   int ndx;

   for (ndx = 0; ndx < n; ++ndx)
      if (paths[n] && strstr(file, paths[ndx]) == file)
         return 1;
   return 0;
}
