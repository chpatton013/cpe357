#include "header.h"

char *permissionStr(char *bf, uint32_t mode) {
   int ndx, pos;
   unsigned int mask;

   for (mask = 1, ndx = 0; ndx < PERM_LEN; mask <<= 1, ++ndx) {
      if (mode & mask) {
         pos = ndx % 3;
         if (pos == 2) bf[PERM_LEN - ndx - 1] = 'r';
         else if (pos == 1) bf[PERM_LEN - ndx - 1] = 'w';
         else bf[PERM_LEN - ndx - 1] = 'x';
      }
      else
         bf[PERM_LEN - ndx - 1] = '-';
   }
   bf[PERM_LEN] = 0;

   return bf;
}

char *timeStr(char *bf, time_t *time) {
   struct tm *timeinfo = (struct tm *)alloca(sizeof(struct tm));
   gmtime_r(time, timeinfo);

   /* Not sure why (and neither is Bob Somers), but the timestamp on these
    * files is off by a little bit. Just about 1900 years - no big deal.
    */
   timeinfo->tm_year += 1900;
   timeinfo->tm_mon += 1;
   timeinfo->tm_mday += -1;
   timeinfo->tm_hour += 16;
   timeinfo->tm_min += 0;

   if (timeinfo->tm_hour > 23) {
      timeinfo->tm_hour -= 24;
      timeinfo->tm_mday += 1;
   }

   /* strftime was writing the weird diamond-question-mark-thing at the end of
    * the string for the root of the directory. ONLY the root folder. So, we
    * use sprintf
    */
   sprintf(bf, "%04d-%02d-%02d %02d:%02d", timeinfo->tm_year, timeinfo->tm_mon,
    timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min);

   return bf;
}

int isEmpty(unsigned char *bf, int n) {
   int ndx;

   for (ndx = 0; ndx < n; ++ndx)
      if (bf[ndx])
         return 0;
   return 1;
}

Header *constructHeader() {
   return (Header*)__malloc(sizeof(Header));
}

int readHeader(Header *h, int fd) {
   unsigned char header[BLOCK_SIZE];
   
   if (read(fd, header, BLOCK_SIZE) != BLOCK_SIZE) {
      perror(execName);
      exit(1);
   }

   if (isEmpty(header, BLOCK_SIZE))
      return 1;

   /* chars */
   h->typeflag = header[TYPEFLAG_OFF];

   /* strings */
   strncpy(h->name, (char*)(header + NAME_OFF), NAME_LEN);
   strncpy(h->linkname, (char*)(header + LINKNAME_OFF), LINKNAME_LEN);
   strncpy(h->magic, (char*)(header + MAGIC_OFF), MAGIC_LEN);
   strncpy(h->version, (char*)(header + VERSION_OFF), VERSION_LEN);
   strncpy(h->uname, (char*)(header + UNAME_OFF), UNAME_LEN);
   strncpy(h->gname, (char*)(header + GNAME_OFF), GNAME_LEN);
   strncpy(h->prefix, (char*)(header + PREFIX_OFF), PREFIX_LEN);

   /* 8-oct ints */
   h->mode = octStrToUInt((char*)(header + MODE_OFF), MODE_LEN);
   h->uid = octStrToUInt((char*)(header + UID_OFF), UID_LEN);
   h->gid = octStrToUInt((char*)(header + GID_OFF), GID_LEN);
   h->chksum = octStrToUInt((char*)(header + CHKSUM_OFF), CHKSUM_LEN);
   h->devmajor = octStrToUInt((char*)(header + DEVMAJOR_OFF), DEVMAJOR_LEN);
   h->devminor = octStrToUInt((char*)(header + DEVMINOR_OFF), DEVMINOR_LEN);

   /* 12-oct ints */
   h->size = octStrToUInt((char*)(header + SIZE_OFF), SIZE_LEN);
   h->mtime = octStrToUInt((char*)(header + MTIME_OFF), MTIME_LEN);

   return 0;
}

void writeHeader(Header *h, int fd) {
   unsigned char header[BLOCK_SIZE];
   char *temp = (char*)alloca(sizeof(char) * STR_BUFFER);

   strncpy((char*)(header + NAME_OFF), h->name, NAME_LEN);

   temp = intToOctStr(h->mode, temp, MODE_LEN, MODE_LEN);
   strncpy((char*)(header + MODE_OFF), temp, MODE_LEN);

   temp = intToOctStr(h->uid, temp, UID_LEN, UID_LEN);
   strncpy((char*)(header + UID_OFF), temp, UID_LEN);

   temp = intToOctStr(h->gid, temp, GID_LEN, GID_LEN);
   strncpy((char*)(header + GID_OFF), temp, GID_LEN);

   temp = intToOctStr(h->size, temp, SIZE_LEN, SIZE_LEN);
   strncpy((char*)(header + SIZE_OFF), temp, SIZE_LEN);

   temp = intToOctStr(h->mtime, temp, MTIME_LEN, MTIME_LEN);
   strncpy((char*)(header + MTIME_OFF), temp, MTIME_LEN);

   strncpy((char*)(header + TYPEFLAG_OFF), &h->typeflag, TYPEFLAG_LEN);

   strncpy((char*)(header + LINKNAME_OFF), h->linkname, LINKNAME_LEN);
   strncpy((char*)(header + MAGIC_OFF), h->magic, MAGIC_LEN);
   strncpy((char*)(header + VERSION_OFF), h->version, VERSION_LEN);
   strncpy((char*)(header + UNAME_OFF), h->uname, UNAME_LEN);
   strncpy((char*)(header + GNAME_OFF), h->gname, GNAME_LEN);

   temp = intToOctStr(h->devmajor, temp, DEVMAJOR_LEN, DEVMAJOR_LEN);
   strncpy((char*)(header + DEVMAJOR_OFF), temp, DEVMAJOR_LEN);

   temp = intToOctStr(h->devminor, temp, DEVMINOR_LEN, DEVMINOR_LEN);
   strncpy((char*)(header + DEVMINOR_OFF), temp, DEVMINOR_LEN);

   strncpy((char*)(header + PREFIX_OFF), h->prefix, PREFIX_LEN);

   if (write(fd, header, BLOCK_SIZE) != BLOCK_SIZE) {
      perror(execName);
      exit(3);
   }
}

void printStandard(Header *h) {
   char *name, *prefix;
   
   name = (char*)alloca(sizeof(char) * (NAME_LEN + 1));
   prefix = (char*)alloca(sizeof(char) * (PREFIX_LEN + 1));

   strcap(name, h->name, NAME_LEN);
   strcap(prefix, h->prefix, PREFIX_LEN);

   if (strlen(prefix))
      printf("%s/", prefix);
   printf("%s\n", name);
}

void printVerbose(Header *h) {
   char str[STR_LEN], *temp = (char*)alloca(sizeof(char) * TEMP_LEN);

   /* permissions */
   if (h->typeflag == TF_DIR)
      str[0] = 'd';
   else if (h->typeflag == TF_LNK)
      str[0] = 'l';
   else
      str[0] = '-';
   str[1] = 0;
   temp = permissionStr(temp, h->mode);
   strcat(str, temp);

   strcat(str, " ");

   /* owner/group */
   strcat(str, h->uname);
   strcat(str, "/");
   strcat(str, h->gname);

   strcat(str, " ");

   /* TODO: determine correct length */
   /* size */
   sprintf(temp, "%8lu", (unsigned long int)h->size);
   strcat(str, temp);

   strcat(str, " ");

   /* mtime */
   temp = timeStr(temp, &h->mtime);
   sprintf(temp, "%s", temp);
   strcat(str, temp);

   /* printf */
   printf("%s ", str);
   printStandard(h);
}

void printHeader(Header *h, int v) {
   if (v) printVerbose(h);
   else printStandard(h);
}
