#include "CFNode.h"

CFNode * constructCFNode(CharFreq *cf) {
   CFNode *cfn = (CFNode*)__malloc(sizeof(CFNode));

   cfn->next = cfn->left = cfn->right = NULL;

   if (cf) {
      cfn->cf = cf;
      cfn->value = cf->freq;
   }

   return cfn;
}

void freeCFNode(CFNode *nd) {
   if (nd)
      free(nd);
}

int cfnComp(const void *ptr1, const void *ptr2) {
   int val;
   CFNode *cfn1 = *(CFNode**)ptr1, *cfn2 = *(CFNode**)ptr2;

   if (!cfn1 || !cfn2) {
      if (!cfn1 && !cfn2)
         return 0;
      else if (!cfn1)
         return -1;
      else if (!cfn2)
         return 1;
   }

   val = cfn1->value - cfn2->value;
   if (val)
      return val;

   return cfComp(&cfn1->cf, &cfn2->cf);
}
