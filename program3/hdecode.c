#include "hdecode.h"

int main(int argc, char **argv) {
   HTable *hTbl;
   int ndx, fdi, fdo;
   char *inFile, *outFile;

   /* error checking */
   if (argc < MIN_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(-1);
   }
   else if (argc > MAX_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(-2);
   }

   /* set file paths */
   if (argc == IN_ARG) {
      inFile = "/dev/stdin";
      outFile = "/dev/stdout";
   }
   else if (argc == OUT_ARG) {
      inFile = !strcmp(argv[IN_ARG], "-") ? "/dev/stdin" : argv[IN_ARG];
      outFile = "/dev/stdout";
   }
   else {
      inFile = !strcmp(argv[IN_ARG], "-") ? "/dev/stdin" : argv[IN_ARG];
      outFile = !strcmp(argv[OUT_ARG], "-") ? "/dev/stdout" : argv[OUT_ARG];
   }

   /* open files */
   fdi = open(inFile, O_RDONLY);
   if (!fdi) {
      perror(inFile);
      exit (-3);
   }
   fdo = open(outFile, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
   if (!fdo) {
      perror(outFile);
      exit (-4);
   }

   /* make hTbl from header */
   hTbl = (HTable*)__malloc(sizeof(HTable));

   hTbl->tbl = constructCFTable();
   hTbl->size = fillCFTableFromHeader(hTbl->tbl, fdi);
   hTbl->tree = makePrefixes(hTbl->tbl);

   for (ndx = 0; ndx < TABLE_SIZE; ++ndx)
      if (hTbl->tbl[ndx]->freq)
         ++hTbl->uniq;

   writeFile(hTbl, fdi, fdo);

   freeHTable(hTbl);

   close(fdi);
   close(fdo);

   return 0;
}

int fillCFTableFromHeader(CharFreq **tbl, int fdi) {
   int ndx, numDiffChars, numCurrChar, count = 0;
   unsigned char currChar;

   read(fdi, &numDiffChars, sizeof(int));
   
   for (ndx = 0; ndx < numDiffChars; ++ndx) {
      read(fdi, &currChar, sizeof(char));
      read(fdi, &numCurrChar, sizeof(int));

      tbl[currChar]->freq = numCurrChar;

      count += numCurrChar;
   }

   return count;
}

void writeFile(HTable *hTbl, int fdi, int fdo) {
   int length, ndx1, ndx2, outNdx = 0, numChars = 0;
   unsigned char ch, inBuffer[INPUT_BUFFER_SIZE], 
    outBuffer[OUTPUT_BUFFER_SIZE];
   CFNode *nd = hTbl->tree->root;

   do {
      length = read(fdi, inBuffer, INPUT_BUFFER_SIZE);
      for (ndx1 = 0; ndx1 < length && numChars < hTbl->size; ++ndx1) {
         ch = inBuffer[ndx1];

         for (ndx2 = 0; (unsigned)ndx2 < (sizeof(char) * BITS_PER_BYTE);
          ++ndx2) {
            if (nd->cf) {
               if (outNdx == OUTPUT_BUFFER_SIZE) {
                  write(fdo, outBuffer, OUTPUT_BUFFER_SIZE);
                  for (outNdx = 0; outNdx < OUTPUT_BUFFER_SIZE; ++outNdx)
                     outBuffer[outNdx] = 0;
                  outNdx = 0;
               }
               outBuffer[outNdx++] = nd->cf->ch;
               ++numChars;
               nd = hTbl->tree->root;
            }
            else
               nd = ch & MSB_MASK ? nd->right : nd->left;
         }
      }
   } while (length > 0 && numChars < hTbl->size);

   write(fdo, outBuffer, outNdx);
}
