#ifndef CF_TREE_H
#define CF_TREE_H

#include <stdlib.h>
#include <alloca.h>

#include "chpatton.h"
#include "CFNode.h"
#include "CFLL.h"

typedef struct CFTree {
   CFNode *root;
   int size;
} CFTree;

CFTree * constructCFTree(CFLL *ll);
void freeCFTree(CFTree *tree);
void makeTreePrefixes(CFTree *tree);

#endif
