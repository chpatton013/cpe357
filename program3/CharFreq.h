#ifndef CHAR_FREQ_H
#define CHAR_FREQ_H

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <alloca.h>
#include <string.h>

#include "chpatton.h"
#include "alloca.h"

#define TABLE_SIZE 256
#define BUFFER_SIZE 1024

typedef struct CharFreq {
   unsigned char ch;
   char *prefix;
   int freq;
} CharFreq;

CharFreq ** constructCFTable();
void freeCFTable(CharFreq **tbl);
int fillCFTable(CharFreq **tbl, int fd);
CharFreq ** copyCFTable(CharFreq **tbl);
void printCFTable(CharFreq **tbl);
int cfComp(const void *ptr1, const void *ptr2);

#endif
