#include "hencode.h"

int main(int argc, char **argv) {
   HTable *hTbl;
   int fdi, fdo;
   char *inFile, *outFile;

   /* error checking */
   if (argc < MIN_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(-1);
   }
   else if (argc > MAX_ARGS) {
      fprintf(stderr, "%s\n", USAGE_MSG);
      exit(-2);
   }

   /* set file paths */
   if (argc == IN_ARG) {
      inFile = "/dev/stdin";
      outFile = "/dev/stdout";
   }
   else if (argc == OUT_ARG) {
      inFile = !strcmp(argv[IN_ARG], "-") ? "/dev/stdin" : argv[IN_ARG];
      outFile = "/dev/stdout";
   }
   else {
      inFile = !strcmp(argv[IN_ARG], "-") ? "/dev/stdin" : argv[IN_ARG];
      outFile = !strcmp(argv[OUT_ARG], "-") ? "/dev/stdout" : argv[OUT_ARG];
   }

   /* open files */
   fdi = open(inFile, O_RDONLY);
   if (!fdi) {
      perror(inFile);
      exit (-3);
   }
   fdo = open(outFile, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
   if (!fdo) {
      perror(outFile);
      exit (-4);
   }

   hTbl = constructHTable(fdi);

   writeHeader(hTbl, fdo);

   /* rewind input */
   close(fdi);
   fdi = open(inFile, O_RDONLY);
   if (!fdi) {
      perror(inFile);
      exit (-3);
   }

   if (hTbl->uniq > 1)
      writeFile(hTbl, fdi, fdo);

   freeHTable(hTbl);

   close(fdi);
   close(fdo);

   return 0;
}

int min(int i1, int i2) {
   return i1 < i2 ? i1 : i2;
}

int max(int i1, int i2) {
   return i1 < i2 ? i2 : i1;
}

char * intToBinStr(int byteBin, int byteLen) {
   int ndx, mask = 1;
   char *str = (char*)__malloc(sizeof(char) * (byteLen + 1));

   str[byteLen] = 0;

   for (ndx = byteLen - 1; ndx >= 0; --ndx, mask *= 2)
      str[ndx] = (byteBin & mask) + '0';

   return str;
}

unsigned int binStrToInt(char *str) {
   int ndx;
   unsigned int num;

   for (ndx = 0; str[ndx]; ++ndx)
      if (str[ndx] != '0' && str[ndx] != '1')
         return 0;

   for (num = 0, ndx = 0; str[ndx]; ++ndx) {
      num <<= 1;
      num |= (str[ndx] - '0');
   }

   return num;
}

void writeHeader(HTable *hTbl, int fdo) {
   int ndx1, ndx2;
   unsigned char buffer[HEADER_BUFFER_SIZE];

   /* init buffer */
   for (ndx1 = 0; ndx1 < HEADER_BUFFER_SIZE; ++ndx1)
      buffer[ndx1] = 0;

   *buffer = hTbl->uniq;
   ndx2 = sizeof(hTbl->uniq);
   for (ndx1 = 0; ndx1 < TABLE_SIZE; ++ndx1) {
      if (hTbl->tbl[ndx1]->freq) {
         buffer[ndx2] = hTbl->tbl[ndx1]->ch;
         ndx2 += sizeof(hTbl->tbl[ndx1]->ch);
         buffer[ndx2] = hTbl->tbl[ndx1]->freq;
         ndx2 += sizeof(hTbl->tbl[ndx1]->freq);
      }
   }

   write(fdo, buffer, ndx2);
}

void writeFile(HTable *hTbl, int fdi, int fdo) {
   unsigned char inBuffer[INPUT_BUFFER_SIZE];
   unsigned char outBuffer[OUTPUT_BUFFER_SIZE + 1];
   int inNdx, outNdx, chNdx, length, codeLen = 0, byteLen = 0,
    shift, temp;
   unsigned int codeBin = 0, byteBin = 0;
   char *prefix;

   /* empty outBuffer */
   for (outNdx = 0; outNdx < OUTPUT_BUFFER_SIZE; ++outNdx)
      outBuffer[outNdx] = 0;
   outNdx = 0;

   /* for each 'buffer' in file */
   do {
      /* empty inBuffer */
      for (inNdx = 0; inNdx < INPUT_BUFFER_SIZE; ++inNdx)
         inBuffer[inNdx] = 0;
      inNdx = 0;

      length = read(fdi, inBuffer, INPUT_BUFFER_SIZE);

      /* for each char in buffer */
      for (chNdx = 0; chNdx < length; ++chNdx) {
         if (outNdx == OUTPUT_BUFFER_SIZE) {
            write(fdo, outBuffer, OUTPUT_BUFFER_SIZE);
            /* empty outBuffer */
            for (outNdx = 0; outNdx < OUTPUT_BUFFER_SIZE; ++outNdx)
               outBuffer[outNdx] = 0;
            outNdx = 0;
         }

         prefix = hTbl->tbl[inBuffer[chNdx]]->prefix;

         codeLen = strlen(prefix);
         codeBin = binStrToInt(prefix);

         /* extra room in byte */
         if ((unsigned)(byteLen + codeLen) < sizeof(char) * BITS_PER_BYTE) {
            byteBin <<= codeLen;
            byteLen += codeLen;
            byteBin |= codeBin;
         }
         /* just enough or not enough */
         else {
            while (codeLen) {
               shift = min((sizeof(char) * BITS_PER_BYTE) - byteLen, codeLen);
               byteBin <<= shift;
               byteLen += shift;
               temp = codeBin >> (codeLen - shift);
               byteBin |= temp;

               if (byteLen == (sizeof(char) * BITS_PER_BYTE)) {
                  outBuffer[outNdx++] = byteBin;
                  byteBin = byteLen = 0;
               }

               temp <<= (codeLen - shift);
               codeBin -= temp;
               codeLen -= shift;
            }
         }
      }
   } while (length > 0);

   /* pad with zeros */
   byteBin <<= ((sizeof(char) * BITS_PER_BYTE) - byteLen);
   if (byteLen)
      outBuffer[outNdx++] = byteBin;

   write(fdo, outBuffer, outNdx);
}
