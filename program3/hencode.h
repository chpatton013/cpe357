#ifndef HENCODE_H
#define HENCODE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "CharFreq.h"
#include "CFTree.h"
#include "HTable.h"

#define USAGE_MSG "usage: hencode [[infile | -] [outfile | -]]"
#define MIN_ARGS 1
#define MAX_ARGS 3
#define IN_ARG 1
#define OUT_ARG 2
#define HEADER_BUFFER_SIZE 1288
#define WRITE_HTABLE_SIZE 2
#define INPUT_BUFFER_SIZE 4096
#define OUTPUT_BUFFER_SIZE 4096
#define BITS_PER_BYTE 8

char * intToBinStr(int byteBin, int byteLen);
unsigned int binStrToInt(char *str);
void writeHeader(HTable *hTbl, int fdi);
void writeFile(HTable *hTbl, int fdi, int fdo);

#endif
