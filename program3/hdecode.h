#ifndef HDECODE_H
#define HDECODE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "CharFreq.h"
#include "CFNode.h"
#include "CFTree.h"
#include "HTable.h"

#define USAGE_MSG "usage: hdecode [[infile | -] [outfile | -]]"
#define MIN_ARGS 1
#define MAX_ARGS 3
#define IN_ARG 1
#define OUT_ARG 2
#define HEADER_BUFFER_SIZE 1288
#define INPUT_BUFFER_SIZE 4096
#define OUTPUT_BUFFER_SIZE 4096
#define BITS_PER_BYTE 8
#define MSB_MASK 0x80

int fillCFTableFromHeader(CharFreq **tbl, int fdi);
void writeFile(HTable *hTbl, int fdi, int fdo);

#endif
