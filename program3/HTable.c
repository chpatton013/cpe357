#include "HTable.h"

CFTree * makePrefixes(CharFreq **tbl) {
   int ndx1, ndx2, tblSize;
   CFNode **arr;
   CFLL *ll;
   CFTree *tree;

   /* preserve original alphabetized tbl */
   tbl = copyCFTable(tbl);
   qsort(tbl, TABLE_SIZE, sizeof(CharFreq*), cfComp);

   for (ndx1 = 0; ndx1 < TABLE_SIZE; ++ndx1)
      if (tbl[ndx1]->freq)
         break;
   tblSize = TABLE_SIZE - ndx1;

   arr = (CFNode**)alloca(sizeof(CFNode*) * tblSize);
   for (ndx2 = 0; ndx2 < tblSize; ++ndx1, ++ndx2)
      arr[ndx2] = constructCFNode(tbl[ndx1]);

   free(tbl);

   ll = constructCFLL(arr, tblSize);
   tree = constructCFTree(ll);

   freeCFLL(ll);

   return tree;
}

HTable * constructHTable(int fd) {
   int ndx;
   HTable * hTbl = (HTable*)__malloc(sizeof(HTable));

   hTbl->tbl = constructCFTable();
   hTbl->size = fillCFTable(hTbl->tbl, fd);
   hTbl->tree = makePrefixes(hTbl->tbl);

   for (ndx = 0; ndx < TABLE_SIZE; ++ndx)
      if (hTbl->tbl[ndx]->freq)
         ++hTbl->uniq;

   return hTbl;
}

void freeHTable(HTable *hTbl) {
   freeCFTable(hTbl->tbl);
   freeCFTree(hTbl->tree);
   free(hTbl);
}
