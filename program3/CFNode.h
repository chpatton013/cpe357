#ifndef CF_NODE_H
#define CF_NODE_H

#include <stdlib.h>
#include <alloca.h>

#include "chpatton.h"
#include "CharFreq.h"

typedef struct CFNode {
   CharFreq *cf;
   struct CFNode *next, *left, *right;
   int value;
} CFNode;

CFNode * constructCFNode(CharFreq *cf);
void freeCFNode(CFNode *nd); /* does not free contained cf */
int cfnComp(const void *ptr1, const void *ptr2);

#endif
