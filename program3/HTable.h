#ifndef HTABLE_H
#define HTABLE_H

#include <stdlib.h>

#include "chpatton.h"
#include "CharFreq.h"
#include "CFNode.h"
#include "CFLL.h"
#include "CFTree.h"

typedef struct HTable {
   int size, uniq;
   CharFreq **tbl;
   CFTree *tree;
} HTable;

HTable * constructHTable(int fd);
void freeHTable(HTable *hTbl);
CFTree * makePrefixes(CharFreq **tbl);

#endif
