#ifndef CF_LL_H
#define CF_LL_H

#include "chpatton.h"
#include "CFNode.h"

typedef struct CFLL {
   CFNode *head;
   int size;
} CFLL;

CFLL * constructCFLL(CFNode **arr, int size);
void freeCFLL(CFLL *ll); /* does not free nodes */
CFNode * removeCFNode(CFLL *ll); /* return first node; do not free */
void placeCFNode(CFLL *ll, CFNode *node); /* add node to correct pos in list */

#endif
