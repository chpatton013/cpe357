#include "CharFreq.h"

CharFreq * constructCharFreq() {
   CharFreq *cf = (CharFreq*)__malloc(sizeof(CharFreq));
   cf->freq = 0;
   return cf;
}

void freeCharFreq(CharFreq *cf) {
   if (cf) {
      if (cf->prefix)
         free(cf->prefix);
      free(cf);
   }
}

CharFreq ** constructCFTable() {
   int ndx;
   CharFreq **tbl = (CharFreq**)__malloc(sizeof(CharFreq*) * TABLE_SIZE);

   for (ndx = 0; ndx < TABLE_SIZE; ++ndx) {
      tbl[ndx] = constructCharFreq();
      tbl[ndx]->ch = (char)ndx;
   }

   return tbl;
}

void freeCFTable(CharFreq **tbl) {
   int ndx;

   for (ndx = 0; ndx < TABLE_SIZE; ++ndx)
      if (tbl[ndx])
         freeCharFreq(tbl[ndx]);

   free(tbl);
}

int fillCFTable(CharFreq **tbl, int fd) {
   int ndx, length, size = 0;
   unsigned char buffer[BUFFER_SIZE];

   do {
      length = read(fd, buffer, BUFFER_SIZE);
      if (length < 0) {
         perror("Read error");
         exit(-2);
      }

      for (ndx = 0; ndx < length; ++ndx, ++size)
         ++tbl[buffer[ndx]]->freq;
   } while (length);

   return size;
}

CharFreq ** copyCFTable(CharFreq **tbl) {
   int ndx;
   CharFreq **copy = NULL;

   if (tbl) {
      copy = (CharFreq**)__malloc(sizeof(CharFreq*) * TABLE_SIZE);

      for (ndx = 0; ndx < TABLE_SIZE; ++ndx)
         copy[ndx] = tbl[ndx];
   }

   return copy;
}

void printCFTable(CharFreq **tbl) {
   int ndx;

   for (ndx = 0; ndx < TABLE_SIZE; ++ndx)
      if (tbl[ndx] && tbl[ndx]->freq)
         printf("0x%.2x: %s\n", tbl[ndx]->ch, tbl[ndx]->prefix);
}

int cfComp(const void *ptr1, const void *ptr2) {
   int val;
   CharFreq *cf1 = *(CharFreq**)ptr1, *cf2 = *(CharFreq**)ptr2;

   if (!cf1 || ! cf2) {
      if (!cf1 && !cf2)
         return 0;
      else if (!cf1)
         return -1;
      else if (!cf2)
         return 1;
   }

   val = cf1->freq - cf2->freq;
   if (val)
      return val;

   return cf1->ch - cf2->ch;
}

